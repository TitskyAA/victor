package org.example;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ComtradeParser {

    /** Путь к папке */
    private String path;

    /** Название осциллограммы */
    private String name;

    private File cfgFile, datFile;

    /** Содержимое файлов */
    private List<String> cfgFileList;
    private List<String> datFileList;

    /** Коэффицинты */
    private List<Double> aList = new ArrayList<Double>();
    private List<Double> bList = new ArrayList<Double>();

    /** Количество сигналов */
    private int analogNum;

    private Iterator<String> datIterator;

    private final List<Signal<Double>> signals = new ArrayList<Signal<Double>>();


    public void process() {
        String[] str = datIterator.next().split(",");

        for (int i = 2; i < analogNum + 2; i++) {
            double sig = Double.parseDouble(str[i]);
            double a = aList.get(i - 2);
            double b = bList.get(i - 2);

            Signal<Double> signal = signals.get(i - 2);

            signal.setValue((sig * a + b) * 1000);
        }
    }

    /** Файл Dat закончен */
    public boolean fileIsOver() { return !datIterator.hasNext(); }

    /** Задать текущий Comtrade */
    public void setComtrade(String path, String name) {
        this.path = path;
        this.name = name;

        cfgFile = new File(path + "\\" + name + ".cfg");
        datFile = new File(path + "\\" + name + ".dat");

        System.out.println("CFG is exists: " + cfgFile.exists());
        System.out.println("DAT is exists: " + datFile.exists());

        try {
            cfgFileList = FileUtils.readLines(cfgFile);
        } catch (IOException e) {
            System.out.println("Не удается найти файл: " + cfgFile.getName());
        }

        try {
            datFileList = FileUtils.readLines(datFile);
        } catch (IOException e) {
            System.out.println("Не удается найти файл: " + datFile.getName());
        }

        analogNum = Integer.parseInt(cfgFileList.get(1).split(",")[1].replace("A", ""));

        for (int i = 2; i < analogNum + 2; i++) {
            double a = Double.parseDouble(cfgFileList.get(i).split(",")[5]);
            double b = Double.parseDouble(cfgFileList.get(i).split(",")[6]);

            aList.add(a);
            bList.add(b);

            signals.add(new Signal<Double>());
        }

        datIterator = datFileList.listIterator();
    }

    public List<Signal<Double>> getSignals() { return signals; }
}
