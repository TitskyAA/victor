package org.example;

public class Sequences {
    private Signal<Double> magZeroSequence = new Signal<>();

    public void process(double magA, double angA, double magB, double angB, double magC, double angC) {
        double ortXZeroSequence = (magA * Math.cos(angA) + magB * Math.cos(angB) + magC * Math.cos(angC));
        double ortYZeroSequence = (magA * Math.sin(angA) + magB * Math.sin(angB) + magC * Math.sin(angC));
        magZeroSequence.setValue(Math.sqrt(ortXZeroSequence * ortXZeroSequence + ortYZeroSequence * ortYZeroSequence) / 3);
    }

    public Signal<Double> getMagZeroSequence() {
        return magZeroSequence;
    }
}
