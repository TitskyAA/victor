package org.example;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class Graphic {

    private final HashMap<Signal<?>, XYSeriesCollection> datasets = new HashMap<>();
    private final CombinedDomainXYPlot plot = new CombinedDomainXYPlot(new NumberAxis("Время"));;

    private final double timeStep = 1; // шаг изменения времени
    private double currentTime = 0.0;

    public Graphic(){
        JFreeChart chart = new JFreeChart("Сигналы", plot);
        chart.setBorderPaint(Color.black);
        chart.setBorderVisible(true);
        chart.setBackgroundPaint(Color.white);

        JFrame frame = new JFrame("Сигналы");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new ChartPanel(chart));
        frame.setSize(1024,768);
        frame.setVisible(true);
    }



    public void process(){
        currentTime += timeStep;
        datasets.forEach((a, c) -> {
            if(a.getValue() instanceof Number) ((XYSeries) c.getSeries().get(0)).add(currentTime, (Number) a.getValue());
            else if(a.getValue() instanceof Boolean) ((XYSeries) c.getSeries().get(0)).add(currentTime, (Boolean) a.getValue() ? 1 : 0);
        });
    }

    public void addSignal(String name, Signal<?> attribute){
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series = new XYSeries(name); series.add(0.0, 0.0);
        NumberAxis rangeAxis = new NumberAxis(name); rangeAxis.setAutoRangeIncludesZero(false);
        XYPlot subplot = new XYPlot(dataset, null, rangeAxis, new StandardXYItemRenderer() );
        subplot.setBackgroundPaint(Color.BLACK);
        subplot.setWeight(7);
        plot.add(subplot);
        dataset.addSeries(series);
        datasets.put(attribute, dataset);
    }

}
