package org.example;

public class Protection {

    // Уставка по току 0 последователньости
    private int currentSetting;

    //Уставка по времени
    private int timeSetting;

    //Включение направленности ступени
    private boolean destination;

    private double cosFa;
    private double cosFb;
    private double cosFc;

    //Пуск защиты
    private boolean start;

    //Срабатывание защиты
    private Signal<Boolean> actuation = new Signal<>();

    //Счтчик времени
    private int timeCounter;

    public Protection(int currentSetting, int timeSetting, boolean destination) {
        this.currentSetting = currentSetting;
        this.timeSetting = timeSetting;
        this.destination = destination;
    }

    public void process(double zeroSequenceCurrent) {

        if (zeroSequenceCurrent > currentSetting) {
            timeCounter++;
            start = true;
        } else {
            timeCounter = 0;
            start = false;
        }
        if (timeCounter >= timeSetting && start) {

            if (destination) {
                if (cosFa < 0 || cosFb < 0 || cosFc < 0) {
                    actuation.setValue(true);
                }
            } else {
                actuation.setValue(true);
            }
        }
    }

    public boolean isStart() {
        return start;
    }

    public Signal<Boolean> getActuation() {
        return actuation;
    }

    public void setCosFa(double cosFa) {
        this.cosFa = cosFa;
    }

    public void setCosFb(double cosFb) {
        this.cosFb = cosFb;
    }

    public void setCosFc(double cosFc) {
        this.cosFc = cosFc;
    }
}
