package org.example;

public class Main {

    /** Путь к папке */
//    private static String path = "C:\\Users\\Виктор\\Desktop\\3 семестр\\алгоритмы\\лаба\\Опыты\\Опыты";
    private static String path = "D:\\Виктору\\tnznp\\Опыты";

    /** Название осциллограммы */
    private static String name = "KZ4";

    public static void main(String[] args) {

        Graphic graphic = new Graphic();
        Sequences sequences = new Sequences();
        Destination destination = new Destination();
        Protection protection1 = new Protection(50, 100, false);
        Protection protection2 = new Protection(50, 100, true);


        ComtradeParser comtradeParser = new ComtradeParser();
        comtradeParser.setComtrade(path, name);

        //Фильтры Фурье для расчета действующего значения тока и его ушла
        MyFurier phsAFilter = new MyFurier(1);
        MyFurier phsBFilter = new MyFurier(1);
        MyFurier phsCFilter = new MyFurier(1);

        //Фильтры Фурье для расчета угла напряжения
        MyFurier phsA_U_Filter = new MyFurier(1);
        MyFurier phsB_U_Filter = new MyFurier(1);
        MyFurier phsC_U_Filter = new MyFurier(1);

        graphic.addSignal("Inst value phsA", comtradeParser.getSignals().get(3));
        graphic.addSignal("Inst value phsB", comtradeParser.getSignals().get(4));
        graphic.addSignal("Inst value phsC", comtradeParser.getSignals().get(5));

//        graphic.addSignal("U Inst value phsA", comtradeParser.getSignals().get(0));
//        graphic.addSignal("U Inst value phsB", comtradeParser.getSignals().get(1));
//        graphic.addSignal("U Inst value phsC", comtradeParser.getSignals().get(2));

//        graphic.addSignal("RMS value phsA", phsAFilter.getModule());
//        graphic.addSignal("RMS value phsB", phsBFilter.getModule());
//        graphic.addSignal("RMS value phsC", phsCFilter.getModule());

        graphic.addSignal("ZeroSequence", sequences.getMagZeroSequence());

        graphic.addSignal("Protection", protection1.getActuation());
        graphic.addSignal("Protection", protection2.getActuation());

        graphic.addSignal("cosFA", destination.getCosFA());
        graphic.addSignal("cosFB", destination.getCosFB());
        graphic.addSignal("cosFC", destination.getCosFC());

        while(!comtradeParser.fileIsOver()) {
            comtradeParser.process();

            Signal<Double> phA_U = comtradeParser.getSignals().get(2);
            Signal<Double> phB_U = comtradeParser.getSignals().get(1);
            Signal<Double> phC_U = comtradeParser.getSignals().get(0);

            Signal<Double> phsA = comtradeParser.getSignals().get(3);
            Signal<Double> phsB = comtradeParser.getSignals().get(4);
            Signal<Double> phsC = comtradeParser.getSignals().get(5);

            phsAFilter.process(phsA.getValue());
            phsBFilter.process(phsB.getValue());
            phsCFilter.process(phsC.getValue());

            phsA_U_Filter.process(phA_U.getValue());
            phsB_U_Filter.process(phB_U.getValue());
            phsC_U_Filter.process(phC_U.getValue());

            //----------------------------------------------------------------------------------------------------------

            destination.setAngIA(phsAFilter.getAngle().getValue());
            destination.setAngIB(phsBFilter.getAngle().getValue());
            destination.setAngIC(phsCFilter.getAngle().getValue());

            destination.setAngUA(phsA_U_Filter.getAngle().getValue());
            destination.setAngUB(phsB_U_Filter.getAngle().getValue());
            destination.setAngUC(phsC_U_Filter.getAngle().getValue());

            destination.process();

            //----------------------------------------------------------------------------------------------------------

            sequences.process(phsAFilter.getModule().getValue(), phsAFilter.getAngle().getValue(),
                                phsBFilter.getModule().getValue(), phsBFilter.getAngle().getValue(),
                                phsCFilter.getModule().getValue(), phsCFilter.getAngle().getValue());

            protection2.setCosFa(destination.getCosFA().getValue());
            protection2.setCosFb(destination.getCosFB().getValue());
            protection2.setCosFc(destination.getCosFC().getValue());

            protection1.process(sequences.getMagZeroSequence().getValue());
            protection2.process(sequences.getMagZeroSequence().getValue());
            graphic.process();
        }






    }
}