package org.example;

public class Destination {

    private double angIA;
    private double angIB;
    private double angIC;
    private double angUA;
    private double angUB;
    private double angUC;

    private Signal<Double> cosFA = new Signal<>();
    private Signal<Double> cosFB = new Signal<>();
    private Signal<Double> cosFC = new Signal<>();

    public void process() {
        cosFA.setValue(Math.cos(angUA - angIA));
        cosFB.setValue(Math.cos(angUB - angIB));
        cosFC.setValue(Math.cos(angUC - angIC));
    }

    public double getAngIA() {
        return angIA;
    }

    public void setAngIA(double angIA) {
        this.angIA = angIA;
    }

    public double getAngIB() {
        return angIB;
    }

    public void setAngIB(double angIB) {
        this.angIB = angIB;
    }

    public double getAngIC() {
        return angIC;
    }

    public void setAngIC(double angIC) {
        this.angIC = angIC;
    }

    public double getAngUA() {
        return angUA;
    }

    public void setAngUA(double angUA) {
        this.angUA = angUA;
    }

    public double getAngUB() {
        return angUB;
    }

    public void setAngUB(double angUB) {
        this.angUB = angUB;
    }

    public double getAngUC() {
        return angUC;
    }

    public void setAngUC(double angUC) {
        this.angUC = angUC;
    }

    public Signal<Double> getCosFA() {
        return cosFA;
    }

    public void setCosFA(Signal<Double> cosFA) {
        this.cosFA = cosFA;
    }

    public Signal<Double> getCosFB() {
        return cosFB;
    }

    public void setCosFB(Signal<Double> cosFB) {
        this.cosFB = cosFB;
    }

    public Signal<Double> getCosFC() {
        return cosFC;
    }

    public void setCosFC(Signal<Double> cosFC) {
        this.cosFC = cosFC;
    }
}
