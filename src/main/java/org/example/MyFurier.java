package org.example;

public class MyFurier {

    private static final int size = 80;
    private float k = (float) (Math.sqrt(2) / size);
    private double[] sin = new double[size];
    private double[] cos = new double[size];
    private int count = 0;
    private double[] buffX = new double[size];
    private double[] buffY = new double[size];
    private double sumX = 0;
    private double sumY = 0;

    private Signal<Double> module = new Signal<>();

    private Signal<Double> angle = new Signal<>();

    public MyFurier(int n) {
        for (int i=0; i< size; i++) {
            sin[i] = Math.sin(n * 2 * Math.PI * i / size);
            cos[i] = Math.cos(n * 2 * Math.PI * i / size);
        }
    }

    public void process(double inst) {

        module(inst);
        angle(inst);
        if (++count > size - 1) { count = 0; }
    }

    public void module(double inst) {

        double x = inst * sin[count];
        double y = inst * cos[count];

        sumX = sumX + (x-buffX[count]); buffX[count] = x;
        sumY = sumY + (y-buffY[count]); buffY[count] = y;

        module.setValue(Math.sqrt((k*sumX*k*sumX + k*sumY*k*sumY)));
    }

    public void angle(double inst) {

        double x = inst * sin[count];
        double y = inst * cos[count];

        sumX = sumX + (x-buffX[count]); buffX[count] = x;
        sumY = sumY + (y-buffY[count]); buffY[count] = y;

        angle.setValue(Math.atan2(k*sumY, k*sumX));
    }

    public Signal<Double> getModule() {
        return module;
    }

    public Signal<Double> getAngle() {
        return angle;
    }
}
